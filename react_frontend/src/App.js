import { Component } from "react";
import { Route, Switch, Redirect } from "react-router-dom";

// importing the all components
import LoginForm from "./components/LoginForm";
import Home from "./components/Home";
import Products from "./components/Products";
import ProductItemDetails from "./components/ProductItemDetails";
import Cart from "./components/Cart";
import NotFound from "./components/NotFound";
import ProtectedRoute from "./components/ProtectedRoute";
import CartContext from "./context/CartContext";
import SignUp from "./components/SignUp";
import Froget from "./components/Forget";
import Payment from "./components/Payment";


import "./App.css";
import PaymentOngoing from "./components/PaymentOngoing";

// importing the css properties
import "./App.css";

/**
 * writing the functions 
 * removeAllCartItems : - removing the all cart items in a cart,
 * incrementCartItemQuantity :- incresse the cart items by click the increment button, 
 * decrementCartItemQuantity :- decrese the cart items by click the decrement button
 * removeCartItem : removing the cart items by using remove button
 * addCartItem :adding the cart items to the cart
 * totalPriceCallback : calculating the price of the all itames
 * call all  fuctions in a return and passing to the value
 * Providing the all routes and passing components
 * some components have protect-route for authentication
 */
class App extends Component {
  state = {
    cartList: [],
    totalPrice: 0
  };

  removeAllCartItems = () => {
    this.setState({ cartList: [] });
  };

  incrementCartItemQuantity = (id) => {
    this.setState((prevState) => ({
      cartList: prevState.cartList.map((eachCartItem) => {
        if (id === eachCartItem.id) {
          const updatedQuantity = eachCartItem.quantity + 1;
          return { ...eachCartItem, quantity: updatedQuantity };
        }
        return eachCartItem;
      }),
    }));
  };

  decrementCartItemQuantity = (id) => {
    const { cartList } = this.state;
    const productObject = cartList.find(
      (eachCartItem) => eachCartItem.id === id
    );
    if (productObject.quantity > 1) {
      this.setState((prevState) => ({
        cartList: prevState.cartList.map((eachCartItem) => {
          if (id === eachCartItem.id) {
            const updatedQuantity = eachCartItem.quantity - 1;
            return { ...eachCartItem, quantity: updatedQuantity };
          }
          return eachCartItem;
        }),
      }));
    } else {
      this.removeCartItem(id);
    }
  };

  removeCartItem = (id) => {
    const { cartList } = this.state;
    const updatedCartList = cartList.filter(
      (eachCartItem) => eachCartItem.id !== id
    );
    this.setState({ cartList: updatedCartList });
  };

  addCartItem = (product) => {
    const { cartList } = this.state;
    const productObject = cartList.find(
      (eachCartItem) => eachCartItem.id === product.id
    );
    if (productObject) {
      this.setState((prevState) => ({
        cartList: prevState.cartList.map((eachCartItem) => {
          if (productObject.id === eachCartItem.id) {
            const updatedQuantity = eachCartItem.quantity + product.quantity;

            return { ...eachCartItem, quantity: updatedQuantity };
          }

          return eachCartItem;
        }),
      }));
    } else {
      const updatedCartList = [...cartList, product];
      this.setState({ cartList: updatedCartList });
    }
  };

  totalPriceCallback = (total) =>{
    this.setState({totalPrice: total})
  }

  render() {
    const { cartList,totalPrice } = this.state;

    return (

      <CartContext.Provider
        value={{
          cartList,
          addCartItem: this.addCartItem,
          removeCartItem: this.removeCartItem,
          incrementCartItemQuantity: this.incrementCartItemQuantity,
          decrementCartItemQuantity: this.decrementCartItemQuantity,
          removeAllCartItems: this.removeAllCartItems,
          totalPriceCallback: this.totalPriceCallback,
          totalPrice
        }}
      >
        
        <Switch>
        
          <Route exact path="/login" component={LoginForm} />
          <Route exact path="/signup" component={SignUp} />
          <Route exact path="/forget" component={Froget} />
          <ProtectedRoute exact path="/" component={Home} />
          <ProtectedRoute exact path="/products" component={Products} />
          <ProtectedRoute
            exact
            path="/products/:id"
            component={ProductItemDetails}
          />
          <ProtectedRoute exact path="/cart" component={Cart} />
          <ProtectedRoute exact path="/payment_page" component={PaymentOngoing}/>
          <ProtectedRoute exact path="/payment_success" component={Payment}/>
          <Route path="/not-found" component={NotFound} />
          <Redirect to="/not-found" />
        </Switch>
      </CartContext.Provider>
    );
  }
}

export default App;
