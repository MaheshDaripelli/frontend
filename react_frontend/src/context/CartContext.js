//imported external dependencies
import React from 'react'

//wrote the react context for gathering all the components 
const CartContext = React.createContext({
  cartList: [],
  removeAllCartItems: () => {},
  addCartItem: () => {},
  removeCartItem: () => {},
  incrementCartItemQuantity: () => {},
  decrementCartItemQuantity: () => {},
  totalPrice: 0,
  totalPriceCallback: ()=>{},
})

//exported the context for further actions 
export default CartContext

