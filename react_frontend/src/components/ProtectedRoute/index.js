//imported external dependencies
import {Redirect, Route} from 'react-router-dom'
import Cookie from 'js-cookie'

//written a function of protectedroute using props and token 
//redirect to another component if undefined is obtained 
const ProtectedRoute = props => {
  const token = Cookie.get('jwt_token')
  if (token === undefined) {
    return <Redirect to="/login" />
  }
  return <Route {...props} />
}

//exporting protecedroute module
export default ProtectedRoute
