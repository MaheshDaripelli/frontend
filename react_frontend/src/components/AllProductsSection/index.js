// importing external modules 
import { Component } from "react";
import Cookies from "js-cookie";
//imported internal dependencies
import FiltersGroup from "../FiltersGroup";
import ProductCard from "../ProductCard";
import ProductsHeader from "../ProductsHeader";
//imported CSS files for styling purpose
import "./index.css";

//written category names and added ID for the further actions 
const categoryOptions = [
  {
    name: "Red Meat",
    categoryId: "1",
  },
  {
    name: "Poultry",
    categoryId: "2",
  },
  {
    name: "Pork",
    categoryId: "3",
  },
  {
    name: "Sea-Food",
    categoryId: "4",
  },
];
//written sort options for price 
const sortbyOptions = [
  {
    optionId: "PRICE_HIGH",
    displayText: "Price (High-Low)",
  },
  {
    optionId: "PRICE_LOW",
    displayText: "Price (Low-High)",
  },
];

//Added rating stars and urls for the ratoing for the products 
const ratingsList = [
  {
    ratingId: "4",
    imageUrl: "http://localhost:3000/images/allproductssection/four-star.png",
  },
  {
    ratingId: "3",
    imageUrl: "http://localhost:3000/images/allproductssection/three-star.png",
  },
  {
    ratingId: "2",
    imageUrl: "http://localhost:3000/images/allproductssection/two-star.png",
  },
  {
    ratingId: "1",
    imageUrl: "http://localhost:3000/images/allproductssection/one-star.png",
  },
];

//added status for the actions 
const apiStatusConstants = {
  initial: "INITIAL",
  success: "SUCCESS",
  failure: "FAILURE",
  inProgress: "IN_PROGRESS",
};


//class component for the state and the initial status
class AllProductsSection extends Component {
  state = {
    productsList: [],
    apiStatus: apiStatusConstants.initial,
    activeOptionId: sortbyOptions[0].optionId,
    activeCategoryId: "",
    searchInput: "",
    activeRatingId: "",
  };

  componentDidMount() {
    this.getProducts();
  }

  getProducts = async () => {
    this.setState({
      apiStatus: apiStatusConstants.inProgress,
    });
    const jwtToken = Cookies.get("jwt_token");
    const { activeOptionId, activeCategoryId, searchInput, activeRatingId } =
//The state object is where you store property values that belongs to the component.     
      this.state;
//taking backend url to fetch the data of product details
//In options taking one object which is headers
//In headers taking object which is Authentication, which uses a Bearer Token, it is is an HTTP authentication scheme that involves security tokens 
//method:GET - to get the product section     
    const apiUrl = `http://localhost:3001/posts/data`;
    const options = {
      headers: {
        Authorization: `Bearer ${jwtToken}`,
      },
      method: "GET",
    };

{/*fetching the data if response is ok or status is sucess and the productList is updated ,if status is fail
    the list should not update
   *the Clock component schedules a UI update by calling setState() with an object containing the current time
  
  */}    
  const response = await fetch(apiUrl, options);
    if (response.ok) {
      const fetchedData = await response.json();
      const updatedData = fetchedData.map((product) => ({
        title: product.title,
        rating: product.rating,
        price: product.price,
        id: product._id,
        imageUrl: product.image,
        type: product.type,
      }));
      this.setState({
        productsList: updatedData,
        apiStatus: apiStatusConstants.success,
      });
    } else {
      this.setState({
        apiStatus: apiStatusConstants.failure,
      });
    }
  };

  //changing sort by options for pricing based on id
  changeSortby = (activeOptionId) => {
    this.setState({ activeOptionId }, this.getProducts);
  };

  clearFilters = () => {
    this.setState(
      {
        searchInput: "",
        activeCategoryId: "",
        activeRatingId: "",
      },
      this.getProducts
    );
  };
//changing rating based on id 
  changeRating = (activeRatingId) => {
    this.setState({ activeRatingId }, this.getProducts);
  };
//changing category based on id the category products should display
  changeCategory = (activeCategoryId) => {
    this.setState({ activeCategoryId }, this.getProducts);
  };

  enterSearchInput = () => {
    this.getProducts();
  };

  changeSearchInput = (searchInput) => {
    this.setState({ searchInput });
  };

  renderFailureView = () => (
    <div className="products-error-view-container">
      <img
        src="http://localhost:3000/images/allproductssection/no-products-view.png"
        alt="all-products-error"
        className="products-failure-img"
      />
      <h1 className="product-failure-heading-text">
        Oops! Something Went Wrong
      </h1>
      <p className="products-failure-description">
        We are having some trouble processing your request. Please try again.
      </p>
    </div>
  );

  renderProductsListView = () => {
    let { productsList, activeOptionId } = this.state;
    productsList.sort(()=>Math.random() - 0.5)
    const shouldShowProductsList = productsList.length > 0;

    return shouldShowProductsList ? (
      <div className="all-products-container">
        <ProductsHeader
          activeOptionId={activeOptionId}
          sortbyOptions={sortbyOptions}
          changeSortby={this.changeSortby}
        />

        <ul className="products-list">
          {productsList.map((product) => (
            <ProductCard productData={product} key={product.id} />
          ))}
        </ul>
      </div>
    ) : (
      <div className="no-products-view">
        <img
          src="http://localhost:3000/images/allproductssection/no-products-view.png"
          className="no-products-img"
          alt="no products"
        />
        <h1 className="no-products-heading">No items Found</h1>
        <p className="no-products-description">
          We could not find any items. Try other filters.
        </p>
      </div>
    );
  };

  renderLoadingView = () => (
    <div className="products-loader-container">
      {/* <Loader type="ThreeDots" color="#0b69ff" height="50" width="50" /> */}
    </div>
  );

  renderAllProducts = () => {
    const { apiStatus } = this.state;

    switch (apiStatus) {
      case apiStatusConstants.success:
        return this.renderProductsListView();
      case apiStatusConstants.failure:
        return this.renderFailureView();
      case apiStatusConstants.inProgress:
        return this.renderLoadingView();
      default:
        return null;
    }
  };
//written class for rendering the state 
  render() {
    const { activeCategoryId, searchInput, activeRatingId } = this.state;
//returning all products as per the saerch component
    return (
      <div className="all-products-section">
        <FiltersGroup
          searchInput={searchInput}
          categoryOptions={categoryOptions}
          ratingsList={ratingsList}
          changeSearchInput={this.changeSearchInput}
          enterSearchInput={this.enterSearchInput}
          activeCategoryId={activeCategoryId}
          activeRatingId={activeRatingId}
          changeCategory={this.changeCategory}
          changeRating={this.changeRating}
          clearFilters={this.clearFilters}
        />
        {this.renderAllProducts()}
      </div>
    );
  }
}

//exporting allproductsection module
export default AllProductsSection;

