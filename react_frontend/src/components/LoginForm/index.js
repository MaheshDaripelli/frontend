//external dependencies
import {Component} from 'react'
import Cookies from 'js-cookie'
import {Link, Redirect} from 'react-router-dom'


//iternal depemdencies
import './index.css'


{/* in login form ,taking the username and password as a string
  *and showing error if user does not fill the details and not following some conditions 
*/}

class LoginForm extends Component {
  state = {
    username: '',
    password: '',
    showSubmitError: false,
    errorMsg: '',
  }

  onChangeUsername = event => {
    this.setState({username: event.target.value})
  }

  onChangePassword = event => {
    this.setState({password: event.target.value})
  }

  onSubmitSuccess = jwtToken => {
    const {history} = this.props

    Cookies.set('jwt_token', jwtToken, {
      expires: 30,
      path: '/',
    })
    history.replace('/')
  }

  onSubmitFailure = data => {
    this.setState({showSubmitError: true, errorMsg:data.status_message})
  }

 //giving backend url to connect the backend data or to view the data for user 

  submitForm = async event => {
    event.preventDefault()
    const {username, password} = this.state
    const userDetails = {username, password}
    const url = 'http://localhost:3001/login/user'
    const options = {
      method: 'POST',
      body: JSON.stringify(userDetails),
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       },
    }
    const response = await fetch(url, options)
    const data = await response.json()
    if (data.status_code !== 200) {
      this.onSubmitFailure(data)
    } else {
      this.onSubmitSuccess(data.jwt_token)
    }
  }

  renderPasswordField = () => {
    const {password} = this.state
    return (
      <>
        <label className="input-label" htmlFor="password">
          PASSWORD
        </label>
        <input
          type="password"
          id="password"
          className="password-input-field"
          value={password}
          onChange={this.onChangePassword}
          placeholder="Password"
        />
      </>
    )
  }

  renderUsernameField = () => {
    const {username} = this.state
    return (
      <>
        <label className="input-label" htmlFor="username">
          USERNAME
        </label>
        <input
          type="text"
          id="username"
          className="username-input-field"
          value={username}
          onChange={this.onChangeUsername}
          placeholder="Username"
        />
      </>
    )
  }
  
 
//The state object is where you store property values that belongs to the component
//When the state object changes, the component re-renders.
//checking jwt token is already defined or not if it  does not defined redirect to previous path
  render() {
    const {showSubmitError, errorMsg} = this.state
    const jwtToken = Cookies.get('jwt_token')
    if (jwtToken !== undefined) {
      return <Redirect to="/" />
    }
    return (
      <div className="login-form-container">
        <img
          src="http://localhost:3000/images/header/logo.png"
          className="login-website-logo-mobile-image"
          alt="website logo"
        />
        <img
          src="http://localhost:3000/images/header/logo.png"
          className="login-image"
          alt="website login"
        />
        <form className="form-container" onSubmit={this.submitForm}>
          <img
            src="https://www.logolynx.com/images/logolynx/fb/fbbe466b9024f642910404ead3f5e4a5.png"
            className="login-website-logo-desktop-image"
            alt="website logo"
          />
          <div className="input-container">{this.renderUsernameField()}</div>
          <div className="input-container">{this.renderPasswordField()}</div>
          <button type="submit" className="login-button">
            Login
          </button>
          <p className='para'>Don't have account?<Link className='login-button1' to = "signup"> <span>Register</span></Link></p>
         
          {showSubmitError && <p className="error-message">*{errorMsg}</p>}

          <Link to= "forget" className='para1'> <p>Forgot Password</p></Link>
        </form>
        
      </div>
    )
  }
}

//exporting loginform module
export default LoginForm
