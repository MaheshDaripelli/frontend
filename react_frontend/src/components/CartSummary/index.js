//imported external dependencies
import { Link } from 'react-router-dom'
//imported internal dependencies
import CartContext from '../../context/CartContext'
//imported CSS file for styling
import './index.css'


const CartSummary = () => (
  <CartContext.Consumer>
    {value => {
      const {cartList,totalPriceCallback} = value
      let total = 0
      const totalPriceContext = ()=>{
        cartList.forEach(eachCartItem => {
          total += eachCartItem.price * eachCartItem.quantity
        })
        totalPriceCallback(total/2)
      }
      cartList.forEach(eachCartItem => {
        total += eachCartItem.price * eachCartItem.quantity
      })

      //returning the component for the user interface
      //used link for the rendering from one component to another component 
      return (
        <>
          <div className="cart-summary-container">
            <h1 className="order-total-value">
              <span className="order-total-label">Order Total:</span> Rs {total}
              /-
            </h1>
            <p className="total-items">{cartList.length} Items in cart</p>
            <Link to="/payment_page" className='link1'>
              <button type="button" onClick={totalPriceContext} className="checkout-button d-sm-none d-lg-flex">
                Checkout
              </button>
            </Link>
          </div>
          <Link to="/payment_page" className='link1'>
            <button type="button" onClick={totalPriceContext} className="checkout-button d-lg-none d-sm-flex">
              Checkout
            </button>
          </Link>
        </>
      )
    }}
  </CartContext.Consumer>
)

//exporting cartsummary module
export default CartSummary
