//external dependencies
import {Link, withRouter} from 'react-router-dom'
//To set or remove the cookies, we are going to use a third-party dependency of react-cookie.
import Cookies from 'js-cookie'


//Internal dependencies
import CartContext from '../../context/CartContext'
import './index.css'

 {/*Remove the token from the client storage to avoid usage
   *modifies the current history entry, replacing it with the stateObj , title , and URL passed in the method parameters using history.replace   
   *props-pass data from one component to another 
*/}

const Header = props => {
  const onClickLogout = async() => {
    const jwtToken = Cookies.get('jwt_token')
    const url = "http://localhost:3001/logout"
    const bodyDetails = {
      user_jwt_token:jwtToken
    }
    const options = {
      method: 'DELETE',
      body: JSON.stringify(bodyDetails),
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       },
    }
    const response = await fetch(url, options)
    const data = await response.json()
    const {history} = props
    Cookies.remove('jwt_token')
    history.replace('/login')
  }

{/*rendering the cart Items count taking cartlist length 
*/}


  const renderCartItemsCount = () => (
    <CartContext.Consumer>
      {value => {
        const {cartList} = value
        const cartItemsCount = cartList.length

        return (
          <>
            {cartItemsCount > 0 ? (
              <span className="cart-count-badge">{cartList.length}</span>
            ) : null}
          </>
        )
      }}
    </CartContext.Consumer>
  )
 {/* for header section using navbar to navigate
    A navbar is a section of a GUI intended to help visitors access your website’s information
   *giving website logo in navbar
   *when we click button we navigate to logout page using onclick()   
*/} 

  return (
    <nav className="nav-header">
      <div className="nav-content">
        <div className="nav-bar-mobile-logo-container">
          <Link to="/">
            <img
              className="website-logo"
              src="http://localhost:3000/images/header/logo.png"
              alt="website logo"
            />
          </Link>

          <button type="button" className="nav-mobile-btn">
            <img
              src="http://localhost:3000/images/header/logout.png"
              alt="nav logout"
              className="nav-bar-image"
              onClick={onClickLogout}
            />
          </button>
        </div>
{/* 
   nav-items
      Home-home page
      products-to navigate all the products
      carts-to navigate cart products and see how many products are added to cart
            and rendering the cart items count
      logout-to navigate log out page      

*/}
        <div className="nav-bar-large-container">
          <Link to="/">
            <img
              className="website-logo"
              src="http://localhost:3000/images/header/logo.png"
              alt="website logo"
            />
          </Link>
          <ul className="nav-menu">
            <li className="nav-menu-item">
              <Link to="/" className="nav-link">
                Home
              </Link>
            </li>

            <li className="nav-menu-item">
              <Link to="/products" className="nav-link">
                Products
              </Link>
            </li>

            <li className="nav-menu-item">
              <Link to="/cart" className="nav-link">
                Cart
                {renderCartItemsCount()}
              </Link>
            </li>
          </ul>
          <button
            type="button"
            className="logout-desktop-btn"
            onClick={onClickLogout}
          >
            Logout
          </button>
          <img src='https://res.cloudinary.com/duwpvypoh/image/upload/v1645741427/20220225_035524_nncqew.jpg' className='profile-image'/>
        </div>
      </div>
      <div className="nav-menu-mobile">
        <ul className="nav-menu-list-mobile">
          <li className="nav-menu-item-mobile">
            <Link to="/" className="nav-link">
              <img
                src="http://localhost:3000/images/header/home.png"
                alt="nav home"
                className="nav-bar-image"
              />
            </Link>
          </li>

          <li className="nav-menu-item-mobile">
            <Link to="/products" className="nav-link">
              <img
                src="http://localhost:3000/images/header/products.png"
                alt="nav products"
                className="nav-bar-image"
              />
            </Link>
          </li>
          <li className="nav-menu-item-mobile">
            <Link to="/cart" className="nav-link">
              <img
                src="http://localhost:3000/images/header/cart-icon.png"
                alt="nav cart"
                className="nav-bar-image"
              />
              {renderCartItemsCount()}
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  )
}


//exporting withRouter(Header) module
export default withRouter(Header)
