//imported internal dependencies 
import CartContext from "../../context/CartContext";
import Header from "../Header";
import EmptyCartView from "../EmptyCartView";
import CartListView from "../CartListView";
import CartSummary from "../CartSummary";
//imported CSS files for styling
import "./index.css";

{/*To keep context re-rendering fast, React needs to make each context consumer a separate node in the tree.
  *removeAllCartItems() is used to remove all cart items when clicking onclick button
  *
*/}

const Cart = () => (
  <CartContext.Consumer>
    {(value) => {
      const { cartList, removeAllCartItems } = value;
      const showEmptyView = cartList.length === 0;
      const onClickRemoveAllBtn = () => {
        removeAllCartItems();
      };
//return component for the cart to display for the user interface
      return (
        <>
          <Header />
          <div className="cart-container">
            {showEmptyView ? (
              <EmptyCartView />
            ) : (
              <div className="cart-content-container">
                <h1 className="cart-heading">My Cart</h1>
                <button
                  type="button"
                  className="remove-all-btn"
                  onClick={onClickRemoveAllBtn}
                >
                  Remove All
                </button>
                <CartListView />
                <CartSummary />
              </div>
            )}
          </div>
        </>
      );
    }}
  </CartContext.Consumer>
);

//exporting cart module
export default Cart;
