//imported CSS file for styling
import './index.css'

//function for showing NOTFOUND content when an invalid path is given in the URL
const NotFound = () => (
  <div className="not-found-container">
    <img
      src="http://localhost:3000/images/notfound/not-found.png"
      alt="not found"
      className="not-found-img"
    />
  </div>
)

//exporting notfound module
export default NotFound
