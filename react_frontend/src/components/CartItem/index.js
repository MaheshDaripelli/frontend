//imported external dependencies
import { BsPlusSquare, BsDashSquare } from "react-icons/bs";
import { AiFillCloseCircle } from "react-icons/ai";
//imported internal dependencies
import CartContext from "../../context/CartContext";
//imported CSS file for styling
import "./index.css";

{/*To keep context re-rendering fast, React needs to make each context consumer a separate node in the tree.
  *Using the functions for increment, decrement cart items and also for removing cart items
  *using props  to pass data from one component to another  
*/}

const CartItem = props => (
  <CartContext.Consumer>
    {(value) => {
      const {
        removeCartItem,
        incrementCartItemQuantity,
        decrementCartItemQuantity,
      } = value;
      const { cartItemDetails } = props;
      let { id, title, brand, quantity, price, imageUrl } = cartItemDetails;
      price = parseInt(price);
      const onClickDecrement = () => {
        decrementCartItemQuantity(id);
      };
      const onClickIncrement = () => {
        incrementCartItemQuantity(id);
      };
      const onRemoveCartItem = () => {
        removeCartItem(id);
      };
      const totalPrice = price * quantity;
//component for returning the component and applicable for UI
      return (
        <li className="cart-item">
          <img className="cart-product-image" src={imageUrl} alt={title} />
          <div className="cart-item-details-container">
            <div className="cart-product-title-brand-container">
              <p className="cart-product-title">{title}</p>
              <p className="cart-product-brand">by {brand}</p>
            </div>
            <div className="cart-quantity-container">
              <button
                type="button"
                className="quantity-controller-button"
                testid="minus"
                onClick={onClickDecrement}
              >
                <BsDashSquare color="#52606D" size={12} />
              </button>
              <p className="cart-quantity">{quantity}</p>
              <button
                type="button"
                className="quantity-controller-button"
                testid="plus"
                onClick={onClickIncrement}
              >
                <BsPlusSquare color="#52606D" size={12} />
              </button>
            </div>
            <div className="total-price-remove-container">
              <p className="cart-total-price">Rs {totalPrice}/-</p>
              <button
                className="remove-button"
                type="button"
                onClick={onRemoveCartItem}
              >
                Remove
              </button>
            </div>
          </div>
          <button
            className="delete-button"
            type="button"
            onClick={onRemoveCartItem}
            testid="remove"
          >
            <AiFillCloseCircle color="#616E7C" size={20} />
          </button>
        </li>
      );
    }}
  </CartContext.Consumer>
);

//exporting cartitem module
export default CartItem;
